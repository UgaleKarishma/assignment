# Client App
**Following steps are taken to setup this project :**

    npm create vite@latest
    //Select the react project with ts option. 
    cd client
    npm install
    npm run dev

### Installing tailwind

    npm install -D tailwindcss postcss autoprefixer
    npx tailwindcss init -p

### To setup flow-bite
Floe-bite is a react library built on top of tailwind css. It provides lot of prebuilt react component to be used as is.

    npm install flowbite flowbite-react

  
### To run the FE project
    npm run dev

# Server App
**To setup the backend application, please following commands were used** 

    npm init -y
    npm install --save-dev typescript @types/node
    npm install express cors body-parser
    npx ts 
 
 ### To setup test environment

    npm i -D jest ts-jest @types/jest
    npm i -D supertest
    npx ts-jest config:init
    
### Update the config file 
        In package.json file did follwing changes for BE
        Inside Script added 
        "test": "jest",
        "start": "npm run dev",
        "dev": "ts-node./src/app.ts"

### To run the BE project
    npm start