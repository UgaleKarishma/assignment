import { useState } from "react";
import "./App.css";
import { TextInput } from "flowbite-react";
import API from "./Api";

function App() {
  const [code, setCode] = useState<Number | any>();
  const [showCode, setShowCode] = useState<String | any>();
  const [character, setCharacter] = useState();
  const [typing, isTyping] = useState<boolean>(false);

  const getCharacter = async (code: Number) => {
    if (code && code != null) {
      setShowCode(code);
      const response: any = await API.getCharByCode(code);
      setCharacter(response?.char);
      isTyping(false);
      setCode("");
    } else {
      alert("Please Provide Valid Number.");
    }
  };

  return (
    <>
      <div className="card">
        <div>
          <h6>Assignment - ASCII CODE TO CHAR</h6>
        </div>

        <div className="flex mt-5">
          <TextInput
            id="code"
            type="number"
            value={code}
            placeholder="Enter code here"
            required={true}
            onChange={(e) => {
              isTyping(true);
              setCode(e.target.value);
            }}
          />
          <button
            onClick={() => getCharacter(code)}
            className="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Get Character
          </button>
        </div>
        {character && !typing && (
          <p className="m-5">
            Result : <br></br>
            The CharCode{" "}
            <b>
              {showCode}
              {code}
            </b>{" "}
            represents Character <b>{character}</b>
          </p>
        )}
      </div>
    </>
  );
}

export default App;
