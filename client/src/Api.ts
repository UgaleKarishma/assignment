 
import axios from "axios";

 const getCharByCode = async (code: Number) => {
    const response= await axios
      .get<String>("http://localhost:8080/character/" + code);
      return response.data;
  };
  


const Api = {
getCharByCode
};

export default Api;