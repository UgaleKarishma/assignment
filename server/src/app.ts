import express from "express";
import { Request, Response } from "express";
import bodyParser from 'body-parser';
import charUtils from './routes/char.routes';
import cors from 'cors';

const port = 8080;
const app = express();
app.use(bodyParser.json());
app.use(cors());

app.get("/", (request: Request, response: Response) => {
  response.send(`Server is running on port ${port}`);
});

app.use('/character', charUtils)


app.get("*", (req: Request, res: Response) => {
  res.send("There is no proper endpoint here");
});

app.listen(port);

export default app;