import express from "express";
import { Request, Response } from "express";

const routes = express.Router();

routes.get("/:code", (req: Request, res: Response) => {
  if (!req.params.code) {
    res.send("Invalid code");
  } else {
    let code = Number.parseInt(req.params.code);
    const character = String.fromCharCode(code);
    res.json({"char": character});
  }
});

export default routes;
